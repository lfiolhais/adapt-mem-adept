# adapt-mem-adept

Adapt an elf or an hex file to be used as memory for an Adept RISC-V implementation.

Makes use of the goblin elf-parsing crate and of the hex crate.
