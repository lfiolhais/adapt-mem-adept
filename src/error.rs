use goblin::error::Error as GoblinError;
use std::fmt;
use std::io::Error as IoError;
use std::result::Result as StdResult;

/// An Error
#[derive(Debug)]
pub enum Error {
    Io(IoError),
    Goblin(GoblinError),
    AdeptBoundsError,
}

/// Conversion from IoError
impl From<IoError> for Error {
    fn from(err: IoError) -> Error {
        Error::Io(err)
    }
}

/// Conversion from GoblinError
impl From<GoblinError> for Error {
    fn from(err: GoblinError) -> Error {
        Error::Goblin(err)
    }
}

/// Display trait implementation for Error
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Io(io_error) => io_error.fmt(f),
            Error::Goblin(goblin_error) => goblin_error.fmt(f),
            Error::AdeptBoundsError => "Attempted to read Adept data out of bounds".fmt(f),
        }
    }
}

/// Result type definition
pub type Result<T> = StdResult<T, Error>;
